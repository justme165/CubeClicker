using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBuffSystem : MonoBehaviour
{
    [SerializeField] Transform _EnemiesParent;
    [SerializeField] Button _killAllButton;
    [SerializeField] Button _spawnerStopButton;
    [SerializeField] EnemySpawn _spawner;

    public void StopSpawningEnemiesFunction()
    {
        _spawner.StopSpawning(3f);
        _spawnerStopButton.interactable = false;
        StartCoroutine(Reload(16f, _spawnerStopButton));
    }

    public void KillAllEnemiesFunction()
    {
        var children = new List<GameObject>();
        foreach (Transform child in _EnemiesParent) children.Add(child.gameObject);
        children.ForEach(child => child.gameObject.GetComponent<EnemyStats>().KillMe());
        _killAllButton.interactable = false;
        StartCoroutine(Reload(18f, _killAllButton));
    }

    IEnumerator Reload(float time, Button funcButton)
    {
        yield return new WaitForSeconds(time);
        funcButton.interactable = true;
    }
}
