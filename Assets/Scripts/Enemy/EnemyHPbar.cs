using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHPbar : MonoBehaviour
{
    MaterialPropertyBlock matBlock;
    MeshRenderer meshRenderer;
    [SerializeField] EnemyStats _enemyStats;
    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        matBlock = new MaterialPropertyBlock();
    }

    void Update()
    {
        if (_enemyStats.HP < _enemyStats.MaxHP)
        {
            meshRenderer.enabled = true;
            CameraAlign();
            UpdateHP();
        }
        else
        {
            meshRenderer.enabled = false;
        }
    }

    private void UpdateHP()
    {
        meshRenderer.GetPropertyBlock(matBlock);
        matBlock.SetFloat("_Fill", _enemyStats.HP / (float)_enemyStats.MaxHP);
        meshRenderer.SetPropertyBlock(matBlock);
    }

    private void CameraAlign()
    {
        if (Camera.main != null)
        {
            var camTransform = Camera.main.transform;
            var forward = transform.position - camTransform.position;
            forward.Normalize();
            var up = Vector3.Cross(forward, camTransform.right);
            transform.rotation = Quaternion.LookRotation(forward, up);
        }
    }
}
