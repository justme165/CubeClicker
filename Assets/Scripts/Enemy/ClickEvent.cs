using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickEvent : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] EnemyStats _thisEnemy;
    public void OnPointerClick(PointerEventData eventData)
    {
        _thisEnemy.TakeDamage();
        //throw new System.NotImplementedException();
    }
}
