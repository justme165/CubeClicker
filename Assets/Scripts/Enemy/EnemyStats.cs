using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStats : MonoBehaviour
{
    [SerializeField] public GameObject _thisPrefab;
    [SerializeField] public NavMeshAgent _thisEnemyAgent;
    [SerializeField] Material enemyColor;
    public int HP;
    public int MaxHP;
    public float Speed;
    public int Value;
    MaterialPropertyBlock matBlock;
    Renderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        //_thisEnemyAgent.enabled = false;
        MaxHP = ScoreIndicator.EnemyHP;
        HP = MaxHP;
        Speed = ScoreIndicator.EnemySpeed;
        Value = ScoreIndicator.EnemyScoreMultiply;

        matBlock = new MaterialPropertyBlock();
        matBlock.SetColor("_Color", new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
        _renderer.SetPropertyBlock(matBlock);

        _thisEnemyAgent.speed += Speed;

    }

    public void KillMe()
    {
        ScoreIndicator.KilledEnemyCount++;
        ScoreIndicator.EnemyCount--;
        ScoreIndicator.LevelScore += Value;
        ScoreIndicator.EnemySpeed += 0.2f;
        GameObject.Destroy(_thisPrefab);
    }

    public void TakeDamage()
    {
        HP--;       
        if (HP <= 0)
        {
            ScoreIndicator.KilledEnemyCount++;
            ScoreIndicator.EnemyCount--;          
            ScoreIndicator.LevelScore += Value;
            ScoreIndicator.EnemySpeed += 0.2f;
            GameObject.Destroy(_thisPrefab);
        }
    }
}
