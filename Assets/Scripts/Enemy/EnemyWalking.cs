using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyWalking : MonoBehaviour
{
    [SerializeField] NavMeshAgent _thisAgent;

    public Vector3 Volume;
    private void Start()
    {
        SetNewPoint();
    }
    private void SetNewPoint()
    {
        Vector3 newDir = new Vector3(Random.Range(-Volume.x, Volume.x), 0, Random.Range(-Volume.z, Volume.z));
        NavMeshHit Hit;
        bool hasDestination = NavMesh.SamplePosition(newDir, out Hit, 100f, -1);
        if (hasDestination)
        {            
            _thisAgent.SetDestination(Hit.position);
        }
    }

    // �������� ���
    void Update()
    {
        //if (_thisAgent.pathPending)
        //{
            if (_thisAgent.remainingDistance <= _thisAgent.stoppingDistance)
            {
                if (!_thisAgent.hasPath || _thisAgent.velocity.sqrMagnitude <= 0.3f)
                {
                    SetNewPoint();
                }
            }
        //}
    }
}
