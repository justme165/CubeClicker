using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIHandler : MonoBehaviour
{
    [SerializeField] Text scoreText;
    [SerializeField] Text enemyCountText;
    private void Update()
    {
        scoreText.text = $"Score: {ScoreIndicator.LevelScore}";
        enemyCountText.text = $"Enemy Count: {ScoreIndicator.EnemyCount}";
    }
}
