using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemySpawn : MonoBehaviour
{
    //[SerializeField] GameObject _EnemyPrefab;
    [SerializeField] EnemyStats _EnemyComponents;
    [SerializeField] GameObject _parent;

    [SerializeField] GameObject _EndGamePanel;
    [SerializeField] Text _KillCountText;
    [SerializeField] Text _TotalScoreText;
    [SerializeField] Text _newHighScoreText;

    private float Duration;

    NavMeshTriangulation Triangulation;

    void Start()
    {
        Triangulation = NavMesh.CalculateTriangulation();
        Invoke(nameof(SpawnEnemies), 3f);
    }
    public void StopSpawning(float time)
    {
        CancelInvoke();
        Invoke(nameof(SpawnEnemies), time);
    }
    private void RandomSpawn()
    {      
        int VertIndex = Random.Range(0, Triangulation.vertices.Length);

        NavMeshHit Hit;
        if (NavMesh.SamplePosition(Triangulation.vertices[VertIndex], out Hit, 3f, -1))
        {
            _EnemyComponents._thisEnemyAgent.Warp(Hit.position);
            _EnemyComponents._thisEnemyAgent.enabled = true;

            GameObject.Instantiate(_EnemyComponents._thisPrefab, _parent.transform);
        }
    }

    private void SpawnEnemies()
    {
        ScoreIndicator.EnemyCount++;
        RandomSpawn();
        if (ScoreIndicator.EnemyCount >= 10)
        {
            EndGame();    
        }

        Duration = Random.Range(2,4);
        Invoke(nameof(SpawnEnemies), Duration);
    }

    private void EndGame()
    {
        CancelInvoke();
        if (ScoreIndicator.LevelScore > ScoreIndicator.HighScore)
        {
            ScoreIndicator.HighScore = ScoreIndicator.LevelScore;
            PlayerPrefs.SetInt("highscore", ScoreIndicator.HighScore);
            _newHighScoreText.text = "����� ������!";
            _newHighScoreText.gameObject.SetActive(true);
        }
            
        
        _KillCountText.text = $"������� �����: {ScoreIndicator.LevelScore}";
        _TotalScoreText.text = $"���������� �����: {ScoreIndicator.KilledEnemyCount}";
        _EndGamePanel.gameObject.SetActive(true);
    }
}
