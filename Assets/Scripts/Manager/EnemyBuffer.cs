using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBuffer : MonoBehaviour
{ 
    // ���������� ���� ��������� �� �������>
    void Start()
    {
        InvokeRepeating(nameof(EnemiesBuff), 8f, 8f);
    }

    private void EnemiesBuff()
    {
        ScoreIndicator.EnemyScoreMultiply ++;        
        if (ScoreIndicator.EnemyHP > 8)
        {
            return;
        }
        else {
            ScoreIndicator.EnemyHP++;
        }
    }
}
