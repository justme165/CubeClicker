using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScoreIndicator : MonoBehaviour
{
    public static int KilledEnemyCount = 0;
    public static int EnemyCount = 0;
    //����� �����������
    public static int EnemyHP = 1;
    public static float EnemySpeed = 1f;
    //����
    public static int LevelScore = 0;
    public static int EnemyScoreMultiply = 1;
    public static int HighScore = 0;

    public static void ClearStatics()
    {
        KilledEnemyCount = 0;
        EnemyCount = 0;
        EnemyHP = 1;
        EnemySpeed = 1f;
        LevelScore = 0;
        EnemyScoreMultiply = 1;
    }
}
