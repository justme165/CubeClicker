using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadRecord : MonoBehaviour
{
    [SerializeField] Text recordText;
    private void Awake()
    {
        if (PlayerPrefs.HasKey("highscore"))
        {
            ScoreIndicator.HighScore = PlayerPrefs.GetInt("highscore");
            recordText.text = $"������: {ScoreIndicator.HighScore}";
        }
        else
            recordText.text = "��� ����������.";


    }
}
